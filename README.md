# Instagram Access

## Dependencies
This project takes the script of crewling the stories from:
https://github.com/notcammy/PyInstaStories

## How to Use
first edit the three variables in instegram_access.py:
```python
your_username = 'yotamenachem'
your_password = 'qwe123!@#'
stories_of = 'lirazkiz'
```

Then just run:

```
python instegram_access.py
```

It will download the relevant stories to `stories/<stories_of>`.
